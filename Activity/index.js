const express = require('express');
const app = express();
const mongoose = require('mongoose');

const port = 3000;
const db = mongoose.connection;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin098@cluster0.khqxuap.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
})

// Connecting to MongoDB locally
db.on("error", console.error.bind(console, "Connection Error \n"));

db.once("open", () => console.log("Connected to MongoDB Atlas!"));

// Create Schema
const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	}
});

const User = mongoose.model("User", userSchema);


// Signup Route

app.post('/signup', (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(err) {
			return console.log(err)
		} else {
			if(result !== null && result.username == req.body.name) {
				return res.send("Username has already exists.");
			} else {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});
				newUser.save((saveErr, savedTask) => {
					if(saveErr) {
						console.error(saveErr);
					} else {
						return res.status(201).send("New user registered!");
					}
				})
			}
		}
	})
})



app.listen(port, () => console.log(`Server is now connected to port ${port}`));