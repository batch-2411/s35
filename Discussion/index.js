const express = require('express');
const mongoose = require('mongoose');
const app = express();

const port = 3000;
let db = mongoose.connection;

// MongoDB Connection
/*
mongoose.connect('<MongoDB connection string>', {useNewUrlParser : true})
*/
// Connecting to MongoDB atlas
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin098@cluster0.khqxuap.mongodb.net/s35?retryWrites=true&w=majority", {
	// Due to update in MongoDB drivers that allow connection to it, the default connection is being flagged as error
	// Allows us to avoid any 
	useNewUrlParser : true,
	useUnifiedTopology: true
})

// Connecting to MongoDB locally
db.on("error", console.error.bind(console, "Connection Error \n"));

db.once("open", () => console.log("Connected to MongoDB Atlas!"));


app.use(express.json());

app.use(express.urlencoded({extended: true}));

// Mongoose Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Models
// Models must be in singular form and capitalized
// first parameter - indicates the collection in where to store data
// second parameter - specify the schema/bluerprint of the documents wthat will be stored in the MongoDB collection	
const Task = mongoose.model("Task", taskSchema);

// Creating a new Task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post('/tasks', (req, res) => {
	// To check if there are duplicate tasks
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result !== null && result.name == req.body.name){
			return res.send("Duplicate task found!");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr) {
				return console.error(saveErr);
			} else {
				return res.status(201).send("New task created!");
			}
			})
		};
	});
});

// Get all the Tasks
/*
Business Logic
Retrieve all the documents
If an error is encountered, print the error
If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			});
		};
	});
});


app.listen(port, () => console.log(`Server is running at port ${port}`));